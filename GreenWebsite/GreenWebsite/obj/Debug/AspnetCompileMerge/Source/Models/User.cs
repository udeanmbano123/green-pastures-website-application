﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using CompareObsolete = System.Web.Mvc.CompareAttribute;
using System.Web;

namespace GreenWebsite.Models
{
    [TrackChanges]
    [Table("User")]
    public class User
    {
        public int UserID { get; set; }




        [Required]
        public String Username { get; set; }



        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        [Index(IsUnique = true)]
        public String Email { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]

        [Display(Name = "Password")]
        [Required]
        public String Password { get; set; }


        [Display(Name = "Confirm password")]
        [Required]
        [CompareObsolete("Password", ErrorMessage = "The password and confirmation password do not match.")]

        public String ConfirmPassword { get; set; }

        [Required]
        [SkipTracking]
        public String FirstName { get; set; }

        [Required]
        [SkipTracking]
        public String LastName { get; set; }

      
        [DisplayName("Member Number")]
        [StringLength(450)]
        [Index(IsUnique = true)]
        [Required]
        public String memberNumber { get; set; }

        [DisplayName("IDNO")]
        [StringLength(450)]
        [Index(IsUnique = true)]
        [Required]
        public String IDNO { get; set; }

        [SkipTracking]
        public Boolean IsActive { get; set; }
        [SkipTracking]
        public DateTime CreateDate { get; set; }

        public virtual ICollection<Role> Roles { get; set; }

    
   

    }
}