﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreenWebsite.Models
{
    public class Events
    {
        public int EventsID { get; set; }
        [Required(ErrorMessage = "Type of Event is required")]
        [Display(Name = "Type of Event")]
        public string EventType { get; set; }
        [Required(ErrorMessage = "Event Description is required")]
        [Display(Name = "Event Description")]
        public string EventDescription { get; set; }
        [Required(ErrorMessage = "Venue is required")]
        [Display(Name = "Venue")]
        public string Venue { get; set; }
        [Required(ErrorMessage = "Start Date is required")]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "End Date is required")]
        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime EndDate { get; set; }

    }
}
