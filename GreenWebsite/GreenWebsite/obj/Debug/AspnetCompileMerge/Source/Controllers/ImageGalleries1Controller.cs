﻿using System;
using System.Collections.Generic;

using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;

using System.Web.Mvc;
using GreenWebsite.DAL;
using GreenWebsite.DAL.security;
using GreenWebsite.Models;



namespace Greenwebsite.Controllers
{
    //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class ImageGalleries1Controller : Controller
    {
        private GreenContext db = new GreenContext();

        // GET: ImageGalleries1
        public async Task<ActionResult> Index()
        {
            var images = db.ImageGallery.ToList();
            return View(images.ToList());
        }

        // GET: ImageGalleries1/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImageGallery imageGallery = await db.ImageGallery.FindAsync(id);
            if (imageGallery == null)
            {
                return HttpNotFound();
            }
            return View(imageGallery);
        }

 
        // POST: ImageGalleries1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
  
        // GET: ImageGalleries1/Edit/5


        // GET: ImageGalleries1/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImageGallery imageGallery = await db.ImageGallery.FindAsync(id);
            if (imageGallery == null)
            {
                return HttpNotFound();
            }
            return View(imageGallery);
        }

        // POST: ImageGalleries1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            ImageGallery imageGallery = await db.ImageGallery.FindAsync(id);
            db.ImageGallery.Remove(imageGallery);
            string fullPath = Request.MapPath("~/Upload_Images/" + imageGallery.Name);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
