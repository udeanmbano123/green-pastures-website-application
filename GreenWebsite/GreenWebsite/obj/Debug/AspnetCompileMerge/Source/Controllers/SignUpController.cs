﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using GreenWebsite.DAL;
using GreenWebsite.Models;
using Database = WebMatrix.Data.Database;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace GreenWebsite.Controllers
{
    public class SignUpController : Controller
    {
        private GreenContext db = new GreenContext();

        // GET: SignUp
     
        // GET: SignUp/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SignUp/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "UserID,Username,Email,Password,ConfirmPassword,FirstName,LastName,IsActive,CreateDate,memberNumber,IDNO")] User user)
        {
            if (ModelState.IsValid)
            {
                var con = Database.Open("Green");
                string IDNO = Request["IDNO"];
                        var brokerCode = "SELECT * FROM CUSTOMER_DETAILS where IDNO='" + IDNO + "'";
                var list12 = con.Query(brokerCode).ToList();
               
                string name = "";
                string lname = "";
                string id = "";
                    string cust = "";
                foreach (var row in list12)
                {
                      name = row.FORENAMES;
                      lname = row.SURNAME;
                      id= row.IDNO;
                      cust = row.CUSTOMER_NUMBER;
                }
                if (IDNO != id)
                {
                    var mod = ModelState.First(c => c.Key == "IDNO");  // this
                    mod.Value.Errors.Add("ID number does not match as per registration");    // this

                    return View(user);
                }
                else if(name!=Request["FirstName"]&&lname!=Request["LastName"])
                {
                    var mod = ModelState.First(c => c.Key == "FirstName");  // this
                    mod.Value.Errors.Add("Names do not match as per registration");
                    // this
                    var mod2 = ModelState.First(c => c.Key == "LastName");  // this
                    mod2.Value.Errors.Add("Last Names do not match as per registration");
                    return View(user);
                }
                else if (cust != Request["memberNumber"])
                {
                    var mod2 = ModelState.First(c => c.Key == "memberNumber"); // this
                    mod2.Value.Errors.Add("Member Number does not match as per registration");
                    return View(user);
                }
                else
                {
                    try
                    {
                        user.Password = Request["Password"].ToString();
                        user.ConfirmPassword = Request["ConfirmPassword"].ToString();
                        user.Password = ComputeHash(user.Password, new SHA256CryptoServiceProvider());
                        user.ConfirmPassword = ComputeHash(user.ConfirmPassword, new SHA256CryptoServiceProvider());
                        user.IsActive = true;
                        user.CreateDate = DateTime.Now;
                        Role role = new Role { RoleName = "User" };
                        user.Roles = new List<Role>();
                        user.Roles.Add(role);
                        db.Users.Add(user);
                        await db.SaveChangesAsync();
                        ViewBag.SuccessMsg = "Account created an email has been sent check your inbox";
                        //Clear The Model
                        //ModelState.Clear();
                        //email
                        //email

                        string messageBody = "<html><body ><p stlyle='color:lightblue;align:justify'>Username :  " + Request["Email"] + "Password:" + Request["Password"].ToString()
                        + "<br/>Pholinjani Road behind Police Mess in Mbabane<br/>" +
                  "P.O.Box A516<br/>" +
                  "Swazi Plaza<br/>" +
                  "MBABANE M101<br/>" +
                  "Tel: +026824049325/+26824041291 Telefax:026824049325,Mobile:+26878061950" +
                  " <img src=cid:myImageID></p></body></ html > ";

                        //create Alrternative HTML view
                        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(messageBody, null, "text/html");

                        //Add Image
                        LinkedResource theEmailImage = new LinkedResource(Server.MapPath("~/images/GreenLogo.jpg.png"));
                        theEmailImage.ContentId = "myImageID";

                        //Add the Image to the Alternate view
                        htmlView.LinkedResources.Add(theEmailImage);
                        var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                        var message = new MailMessage();
                        message.To.Add(new MailAddress(Request["Email"])); // replace with valid value 
                        message.From = new MailAddress("info@greenpastures.co.sz"); // replace with valid value
                        message.Subject = "Green Pastures Login Credentials";
                       message.IsBodyHtml = true;
                        message.AlternateViews.Add(htmlView);
                        using (var smtp = new SmtpClient())
                        {
                            var credential = new NetworkCredential
                            {
                                UserName = "info@greenpastures.co.sz", // replace with valid value
                                Password = "Passwr@2016" // replace with valid value
                            };
                            smtp.Credentials = credential;
                            smtp.Host = "smtp.greenpastures.co.sz";
                            smtp.Port = 587;
                            smtp.EnableSsl = true;
                            ServicePointManager.ServerCertificateValidationCallback =
    delegate (object s, X509Certificate certificate,
             X509Chain chain, SslPolicyErrors sslPolicyErrors)
    { return true; };
                            await smtp.SendMailAsync(message);

                        }
                    }
                    catch (Exception e)
                    {
                        var check = db.Users.ToList().Where(a => a.IDNO == IDNO);
                        var check2 = db.Users.ToList().Where(a => a.Email == Request["Email"]);
                        var check3 = db.Users.ToList().Where(a => a.memberNumber == Request["memberNumber"]);


                        string mail = "";
                        string id2 = "";
                        string cust2 = "";
                        foreach (var row in check)
                        {

                            id2 = row.IDNO;

                        }
                        foreach (var row2 in check2)
                        {
                            mail = row2.Email;

                        }
                        foreach (var row3 in check3)
                        {

                            cust2 = row3.memberNumber;
                        }
                        if (IDNO == id2)
                        {
                            var mod3 = ModelState.First(c => c.Key == "IDNO");  // this
                            mod3.Value.Errors.Add("Duplicate ID numbers are not allowed");

                        }
                        else if (mail == Request["Email"])
                        {
                            var mod = ModelState.First(c => c.Key == "Email");  // this
                            mod.Value.Errors.Add("Duplicate emails are not allowed");    // this

                        }
                        else if (cust2 == Request["memberNumber"])
                        {
                            var mod2 = ModelState.First(c => c.Key == "memberNumber"); // this
                            mod2.Value.Errors.Add("Duplicate member numbers are not allowed");

                        }

                        return View(user);
                    }
                }
                

                ModelState.Clear();
                return View(new User());
            }
           
            return View(user);
        }

        // GET: SignUp/Edit/5
    
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
