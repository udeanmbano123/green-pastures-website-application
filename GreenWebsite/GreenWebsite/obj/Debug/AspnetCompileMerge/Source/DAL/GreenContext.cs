﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using System.Web.Security;
using GreenWebsite.Models;
using TrackerEnabledDbContext.Identity;


namespace GreenWebsite.DAL
{
    public class GreenContext : TrackerIdentityContext<ApplicationUser>
    {
        public GreenContext() : base("GreenContext")
        {
        }
        public DbSet<Support> Supports { get; set; }
        public DbSet<FileDetail> FileDetails { get; set; }
        public DbSet<ImageGallery> ImageGallery { get; set; }
        public DbSet<Events> Events { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //one-to-many relationships

        
            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.ToTable("UserRoles");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

     

        //  public System.Data.Entity.DbSet<investorpage.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}
