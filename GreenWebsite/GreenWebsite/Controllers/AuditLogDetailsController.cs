﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using GreenWebsite.DAL;
using GreenWebsite.DAL.security;
using TrackerEnabledDbContext.Common.Models;
using PagedList;
namespace GreenWebsite.Controllers
{    //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
     // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    public class AuditLogDetailsController : Controller
    {

        private GreenContext db = new GreenContext();
        // GET: AuditLog
        public async Task<ActionResult> Index(string sortOrder, string currentFilter, string searchString, string Date1String, string Date2String,
            int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
           
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.EmailSortParm = String.IsNullOrEmpty(sortOrder) ? "email" : "";
            var LogDetails = db.LogDetails.Include(u=>u.Log);
          
            if (searchString != null || (Date1String != null && Date2String != null))
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
                LogDetails = LogDetails.Where(s =>
                 s.Log.UserName.ToUpper().Contains(searchString.ToUpper())
                 ||
                s.Log.UserName.ToUpper().Contains(searchString.ToUpper()));
            }

            else if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
                LogDetails = from s in db.LogDetails
                             join au in db.AuditLog on s.AuditLogId equals au.AuditLogId
                               where date <= au.EventDateUTC && date2 >= au.EventDateUTC
                                 select s;
           

            }
            switch (sortOrder)
            {
                case "name_desc":
                    LogDetails = LogDetails.OrderByDescending(s => s.PropertyName);
                    break;
        
                case "Date":
                    LogDetails = LogDetails.OrderBy(s => s.Log.EventDateUTC);
                    break;
                case "date_desc":
                    LogDetails = LogDetails.OrderByDescending(s => s.Log.EventDateUTC);
                    break;
                default:
                    LogDetails = LogDetails.OrderBy(s => s.Id);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(LogDetails.ToPagedList(pageNumber, pageSize));
        }

        // GET: AuditLog/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuditLogDetail audit = await db.LogDetails.FindAsync(id);
            if (audit == null)
            {
                return HttpNotFound();
            }
            return View(audit);
        }


        // POST: AuditLog/Create



        // GET: AdminShares/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuditLogDetail audit = await db.LogDetails.FindAsync(id);
            if (audit == null)
            {
                return HttpNotFound();
            }
            return View(audit);
        }

        // POST: AdminShares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AuditLogDetail audit = await db.LogDetails.FindAsync(id);
            db.LogDetails.Remove(audit);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
