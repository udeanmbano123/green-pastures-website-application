﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using GreenWebsite.DAL;
using GreenWebsite.Models;
using System.Data.Entity;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace GreenWebsite.Controllers
{
  
    public class HomeController : Controller
    {
        private GreenContext db = new GreenContext();
        public ActionResult Index()
        {
            return View();
        }

    
        [Route("GreenPastures/About")]
        [Route("Home/About")]
        [Route("About")]
        [Route("GreenPastures/AboutUs")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
      
        [Route("GreenPastures/Contact")]
       [Route("Home/Contact")]
        [Route("Contact")]
        [Route("ContactUs")]
        [Route("SendMessage")]
        [Route("GreenPastures/ContactUs")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [Route("GreenPastures/Contact")]
        [Route("Home/Contact")]
        [Route("Contact")]
        [Route("ContactUs")]
        [Route("SendMessage")]
        [Route("GreenPastures/ContactUs")]
        [HttpPost]
        public async Task<ActionResult> Contact(ContactModels c)
        {
            if (ModelState.IsValid)
            {
                try
                {
                   
                    var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                    string messageBody = "FirstName :  " + c.FirstName + " " + "LastName" + " " +
                                         c.LastName + " "+" "+ c.Email+ " " + " The Comments" + c.Comment;
                    var message = new MailMessage();
                    message.To.Add(new MailAddress("info@greenpastures.co.sz"));  // replace with valid value 
                    message.From = new MailAddress("no-reply@greenpastures.com");  // replace with valid value
                    message.Subject = "Green Pastures Email Notification";
              
                    message.Body = string.Format(body, "Admin", "no-reply@greenpastures.co.sz",

                       "<strong>FirstName</strong> :  " + c.FirstName + "<br/>" + "<strong>LastName</strong>" + " " +
                                         c.LastName + "<br/> " + "<strong>Email:</strong>" + c.Email + "<br/> " + "<strong>Comment</strong> :" + c.Comment );

                    message.IsBodyHtml = true;
                   
                    using (var smtp = new SmtpClient())
                    {
                        var credential = new NetworkCredential
                        {
                            UserName = "info@greenpastures.co.sz",  // replace with valid value
                            Password = "Passwr@2016"  // replace with valid value
                        };
                        smtp.Credentials = credential;
                        smtp.Host = "smtp.greenpastures.co.sz";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        ServicePointManager.ServerCertificateValidationCallback =
   delegate (object s, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors sslPolicyErrors)
   { return true; };
                        await smtp.SendMailAsync(message);
                        c.FirstName = "";
                        c.LastName = "";
                        c.Comment = "";
                        c.Email = "";
                        ViewBag.SuccessMsg = "Message Sent";
                        //Clear The Model
                        ModelState.Clear();
                    }
                }
                catch (Exception)
                {
                    return View("Error");
                }
            }
            return View();
        }
      
        [Route("GreenPastures/Product")]
        [Route("Home/Products")]
        [Route("Product")]
        [Route("Collection")]
        [Route("Products")]
        [Route("GreenPastures/Products")]
        public ActionResult Products()
        {
            ViewBag.Message = "Our Products";
            return View();
        }

        [Route("GreenPastures/MEMBERSHIPSHARES")]
        [Route("Home/Product2")]
        [Route("MEMBERSHIPSHARES")]
        [Route("Product2")]
        [Route("GreenPastures/Product2")]
        public ActionResult Product2()
        {
            ViewBag.Message = "MEMBERSHIP SHARES";
            return View();
        }

        [Route("GreenPastures/LOANS")]
        [Route("Home/LOANS")]
        [Route("LOANS")]
        [Route("Product3")]
        [Route("GreenPastures/Product3")]
        public ActionResult Product3()
        {
            ViewBag.Message = "LOANS";
            return View();
        }
        [Route("GreenPastures/ADDITIONALSERVICES")]
        [Route("Home/ADDITIONAL SERVICES")]
        [Route("ADDITIONALSERVICES")]
        [Route("Product4")]
        [Route("GreenPastures/Product4")]
        public ActionResult Product4()
        {
            ViewBag.Message = "ADDITIONAL SERVICES";
            return View();
        }

        [Route("GreenPastures/CLAIM REQUIREMENTS")]
        [Route("Home/CLAIM REQUIREMENTS")]
        [Route("CLAIM REQUIREMENTS")]
        [Route("Product5")]
        [Route("GreenPastures/Product5")]
        public ActionResult Product5()
        {
            ViewBag.Message = "CLAIM REQUIREMENTS";
            return View();
        }

        public ActionResult FAQs()
        {
            ViewBag.Title = "Frequently Asked Questions";
            return View();
        }

        public ActionResult Gallery()
        {
            var imagesModel = new ImageGallery();
            var imageFiles = Directory.GetFiles(Server.MapPath("~/Upload_Images/"));
            foreach (var item in imageFiles)
            {
                imagesModel.ImageList.Add(Path.GetFileName(item));
            }
            return View(imagesModel);
        }
        public async Task<ActionResult> Downloads()
        {

            return View(await db.FileDetails.ToListAsync());

        }
        public FileResult Download(String p, String d)
        {
            return File(Path.Combine(Server.MapPath("~/Upload_Files"), p), System.Net.Mime.MediaTypeNames.Application.Octet, d);
        }


    }
}