﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using GreenWebsite.DAL.security;
using GreenWebsite.Models;
using GreenWebsite.DAL;
using Newtonsoft.Json;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
namespace investorpage.Controllers
{
    public class AccountController : Controller
    {
        GreenContext Context = new GreenContext();
        //
        // GET: /Account/

        public ActionResult Index()
        {
            return View();
        }


 
       

        [HttpPost]
        public ActionResult Index(LoginViewModel model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                model.Password= ComputeHash(model.Password, new SHA256CryptoServiceProvider());
                var user = Context.Users.Where(u => u.Email == model.Username && u.Password ==model.Password).FirstOrDefault();
                if (user != null)
                {
                    var roles = user.Roles.Select(m => m.RoleName).ToArray();

                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                    serializeModel.UserId = user.UserID;
                    serializeModel.FirstName = user.FirstName;
                    serializeModel.LastName = user.LastName;
                    serializeModel.roles = roles;



                    string userData = JsonConvert.SerializeObject(serializeModel);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                            user.Email,
                             DateTime.Now,
                             DateTime.Now.AddMinutes(60),
                             false,
                             userData);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);

                    if (roles.Contains("Admin"))
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    else if (roles.Contains("User"))
                    {
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }

                }

                ModelState.AddModelError("", "Incorrect username and/or password");
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Account", null);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
        //setting forgot passsword
        public ActionResult ForgotPassword(LoginViewModel model)
        {


            return View();
        }
        [HttpPost]
        public async Task<ActionResult> ForgotPassword(LoginViewModel model, string returnUrl = "")
        {




            var user = Context.Users.Where(u => u.Email == model.Username).FirstOrDefault();
            // this
            if (user != null)
            {

                var mod = ModelState.First(c => c.Key == "Username");  // this
                mod.Value.Errors.Add("Check your inbox an email has been sent with your new password");    // this
                                                                                                           // ViewBag.Message = "In database";

                User userData = Context.Users.Where(u => u.UserID == user.UserID).SingleOrDefault();
                //Update the Database
                //Autogenerate password
                string allowedChars = "";
                allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
                allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
                allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";
                char[] sep = { ',' };
                string[] arr = allowedChars.Split(sep);
                string passwordString = "";
                string password = "";
                string temp = "";
                Random rand = new Random();
                for (int i = 0; i < 14; i++)
                {
                    temp = arr[rand.Next(0, arr.Length)];
                    passwordString += temp;
                }
                password = passwordString;
                string hPassword = ComputeHash(password, new SHA256CryptoServiceProvider());

                userData.Password = hPassword;
                userData.ConfirmPassword = hPassword;

                Context.Entry(userData).State = EntityState.Modified;
                Context.SaveChanges();
                //sending emails
                //content supposition

                string messageBody = "<html><body ><p stlyle='color:lightblue;align:justify'>Username :  " + user.Email + "  Password:" + password
                    + "<br/>Pholinjani Road behind Police Mess in Mbabane<br/>" +
                   "P.O.Box A516<br/>" +
                   "Swazi Plaza<br/>" +
                   "MBABANE M101<br/>" +
                   "Tel: +026824049325/+26824041291 Telefax:026824049325,Mobile:+26878061950" +
                   " <img src=cid:myImageID></p></body></ html > ";

                //create Alrternative HTML view
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(messageBody, null, "text/html");

                //Add Image
                LinkedResource theEmailImage = new LinkedResource(Server.MapPath("~/images/GreenLogo.jpg.png"));
                theEmailImage.ContentId = "myImageID";

                //Add the Image to the Alternate view
                htmlView.LinkedResources.Add(theEmailImage);

                //Add view to the Email Message
              

                var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(user.Email)); // replace with valid value 
                message.From = new MailAddress("info@greenpastures.co.sz"); // replace with valid value
                message.Subject = "Green Pastures Login Credentials";
                //message.Body = string.Format(body, "Admin", "info@greenpastures.co.sz", messageBody);
                message.IsBodyHtml = true;
              message.AlternateViews.Add(htmlView);
                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "info@greenpastures.co.sz", // replace with valid value
                        Password = "Passwr@2016" // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.greenpastures.co.sz";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    ServicePointManager.ServerCertificateValidationCallback =
    delegate (object s, X509Certificate certificate,
             X509Chain chain, SslPolicyErrors sslPolicyErrors)
    { return true; };
                    await smtp.SendMailAsync(message);

                }

            }
            else
            {
                var mod = ModelState.First(c => c.Key == "Username");  // this
                mod.Value.Errors.Add("Email does not exist in our records");
            }




            return View(model);

        }

    }
}