﻿using GreenWebsite.DAL;
using GreenWebsite.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace GreenWebsite.Controllers
{
    public class AdminProfileController : Controller
    {
        private GreenContext db = new GreenContext();

        // GET: UserAccounts
        public async Task<ActionResult> Index()
        {
            string name = WebSecurity.CurrentUserName;
            var user = db.Users.ToList().Where(a => a.Email == name);
            return View(user.ToList());
        }

        // GET: UserAccounts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }




        // GET: UserAccounts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            user.Password = "";
            user.ConfirmPassword = "";
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: UserAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserID,Username,Email,Password,ConfirmPassword,FirstName,LastName,IsActive,CreateDate,,memberNumber")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Password = Request["Password"].ToString();
                user.ConfirmPassword = Request["ConfirmPassword"].ToString();
                user.Password = ComputeHash(user.Password, new SHA256CryptoServiceProvider());
                user.ConfirmPassword = ComputeHash(user.ConfirmPassword, new SHA256CryptoServiceProvider());
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges(WebSecurity.CurrentUserName);
                return RedirectToAction("Index");
            }
            return View(user);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
