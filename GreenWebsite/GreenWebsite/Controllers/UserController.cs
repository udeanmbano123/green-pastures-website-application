﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GreenWebsite.Controllers;
using GreenWebsite.DAL.security;
using System.Web.Security;
using GreenWebsite.DAL;
using PagedList;
using WebMatrix.Data;
using WebMatrix.WebData;

using Database = WebMatrix.Data.Database;
namespace investorpage.Controllers
{
    // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    public class UserController : BaseController
    {
        private GreenContext db = new GreenContext();
       
        //
        // GET: /User/
        public ActionResult Index()
        {
            string name = WebSecurity.CurrentUserName;
            return View();
        }
        public ActionResult Balance(int? page)
        {
            string name = WebSecurity.CurrentUserName;
            var user = from u in db.Users
                       where u.Email == name
                       select u;
            string memberno = "";
            string username = "";
            foreach (var key in user)
            {
                memberno = key.memberNumber;
                username = key.Email;
            }
            Database con2;
            
       var con = Database.Open("Green");

        var account = memberno;
             var selectQueryString= "SELECT isnull((select Acctype_description from para_acctype where Acctype = trans.txn_code ),txn_code) as Description,SUM(amount) AS TotalAmount FROM trans where account_no ='"+ account + "' and Settled='0' GROUP BY txn_code";
            try
            {
                var products = con.Query(selectQueryString);
                    //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?
            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            var onePageOfProducts = products.ToPagedList(pageNumber, 20); // will only contain 25 products max because of the pageSize

            ViewBag.OnePageOfProducts = onePageOfProducts;

            ViewBag.MemberNumber = memberno;
            ViewBag.Username = username;
            //var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            //var onePageOfProducts = selectQueryString.ToPagedList(pageNumber, 25); // will only contain 25 products max because of the pageSize

            //ViewBag.OnePageOfProducts = onePageOfProducts;
            con.Connection.Close();
            con.Close();
            }
            catch (Exception e)
            {
                return RedirectToAction("Balance");
            }
      
            return View();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Statement( int?page,string Search, string Date1String, string Date2String)
        {
            string name = WebSecurity.CurrentUserName;
            var user = from u in db.Users
                       where u.Email == name
                       select u;
            string memberno = "";
            string username = "";
            foreach (var key in user)
            {
                memberno = key.memberNumber;
                username = key.Email;
            }
          
            var con = Database.Open("Green");

            var account = memberno;
            string ddl = Request["ddlname"];
      
            if (!String.IsNullOrEmpty(ddl))
            {
                var selectQueryString ="SELECT txn_code As Category,Description,txn_date As TrxnDate,Amount FROM trans where account_no='" + account + "' AND txn_code='" + ddl + "' and Settled='0' Order by txn_date Asc";

                var products = con.Query(selectQueryString);
                //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?
                var selectQueryString2 ="SELECT distinct  txn_code As Category FROM trans where account_no='" + account + "'";


                //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?

                var pageNumber = page ?? 1;
                // if no page was specified in the querystring, default to the first page (1)
                var onePageOfProducts = products.ToPagedList(pageNumber, 20);
                // will only contain 25 products max because of the pageSize
               
                    ViewBag.OnePageOfProducts = onePageOfProducts;

                    ViewBag.MemberNumber = memberno;

                    // dynamic drop down viewbag
                    string sql = selectQueryString; //I have assigned the query to a string and now i will execute this
                
                    //Here i run the query in db against the table employee you need to change this to products.
                    var list = con.Query(selectQueryString2).ToList();
                
                    //Create List of SelectListItem
                    List<SelectListItem> selectlist = new List<SelectListItem>();
                    selectlist.Add(new SelectListItem() {Text = "", Value = ""});
                    foreach (var row in list)
                    {
                        //Adding every record to list  
                        selectlist.Add(new SelectListItem {Text = row.Category, Value = row.Category.ToString()});
                    }

                    ViewBag.SelectList = selectlist; //Assign list to ViewBag will access this in view
                    ViewBag.Username = username;
                    ViewBag.Category = ddl;

                    ViewBag.StartDate = "";
                    ViewBag.EndDate = "";
                    ViewBag.Date1 = "";

                    ViewBag.Date2 = "";
                    con.Connection.Close();
                    con.Close();
                    return View(list);
              

            }
            else if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
                var selectQueryString ="SELECT txn_code As Category,description,txn_date As TrxnDate,Amount FROM trans where account_no='" + account + "' AND txn_date>='"+ date + "' AND txn_date<='" + date2 + "' and Settled='0' Order by txn_date Asc";

                var selectQueryString2 ="SELECT distinct txn_code As Category FROM trans where account_no='" + account + "'";

                var products = con.Query(selectQueryString);
                //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?

                var pageNumber = page ?? 1;
                // if no page was specified in the querystring, default to the first page (1)
                var onePageOfProducts = products.ToPagedList(pageNumber, 20);
                // will only contain 25 products max because of the pageSize

                ViewBag.OnePageOfProducts = onePageOfProducts;

                ViewBag.MemberNumber = memberno;
                ViewBag.StartDate="Start Date :"+date;

                ViewBag.EndDate = "End Date   :"+date2;
                ViewBag.Date1 = date;

                ViewBag.Date2 = date2;
                ViewBag.Category = "";
              
                // dynamic drop down viewbag
                string sql = selectQueryString;//I have assigned the query to a string and now i will execute this

                //Here i run the query in db against the table employee you need to change this to products.
                var list = con.Query(selectQueryString2).ToList();
                //Create List of SelectListItem
                
                List<SelectListItem> selectlist = new List<SelectListItem>();
                selectlist.Add(new SelectListItem() { Text = "", Value = "" });
                foreach (var row in list)
                {
                    //Adding every record to list  
                    selectlist.Add(new SelectListItem { Text = row.Category, Value = row.Category.ToString() });
                }

                ViewBag.SelectList = selectlist;//Assign list to ViewBag will access this in view
                ViewBag.Username = username;
                con.Connection.Close();
                con.Close();
                return View(list);

            }
            else
            {


                var selectQueryString = "SELECT TOP 10 txn_code As Category,description,txn_date As TrxnDate,Amount  FROM trans where account_no='" + account + "' and Settled='0' Order by txn_code Asc";
                try
                {
                    var products = con.Query(selectQueryString);
                    //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?
                    var selectQueryString2 ="SELECT distinct txn_code As Category FROM trans where account_no='" + account + "'";


                    //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?

                    var pageNumber = page ?? 1;
                    // if no page was specified in the querystring, default to the first page (1)
                    var onePageOfProducts = products.ToPagedList(pageNumber, 20);
                    // will only contain 25 products max because of the pageSize

                    ViewBag.OnePageOfProducts = onePageOfProducts;

                    ViewBag.MemberNumber = memberno;

                    // dynamic drop down viewbag
                    string sql = selectQueryString; //I have assigned the query to a string and now i will execute this

                    //Here i run the query in db against the table employee you need to change this to products.
                    var list = con.Query(selectQueryString2).ToList();
                    //Create List of SelectListItem
                    List<SelectListItem> selectlist = new List<SelectListItem>();
                    selectlist.Add(new SelectListItem() {Text = "", Value = ""});
                    foreach (var row in list)
                    {
                        //Adding every record to list  
                        selectlist.Add(new SelectListItem {Text = row.Category, Value = row.Category.ToString()});
                    }

                    ViewBag.SelectList = selectlist; //Assign list to ViewBag will access this in view
                    ViewBag.Username = username;
                    ViewBag.Category = "";
                    ViewBag.StartDate = "";
                    ViewBag.EndDate = "";
                    ViewBag.Date1 = "";

                    ViewBag.Date2 = "";
                    con.Connection.Close();
                    con.Close();
                    return View(list);
                }
                catch (Exception e)
                {
                    return RedirectToAction("Statement");
                }
          
            }
          

        }
    }
}