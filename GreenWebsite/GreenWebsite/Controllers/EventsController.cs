﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GreenWebsite.DAL;
using GreenWebsite.DAL.security;
using GreenWebsite.Models;

namespace GreenWebsite.Controllers
{
    //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class EventsController : Controller
    {
        private GreenContext db = new GreenContext();

        // GET: Events
        public async Task<ActionResult> Index()
        {
            return View(await db.Events.ToListAsync());
        }

        // GET: Events/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Events events = await db.Events.FindAsync(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        // GET: Events/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "EventsID,EventType,EventDescription,Venue,StartDate,EndDate")] Events events)
        {
            DateTime Date1=new DateTime();
            DateTime Date2 = new DateTime();
            if (ModelState.IsValid)
            {
                Date1 = Convert.ToDateTime(Request["StartDate"]);
                Date2 = Convert.ToDateTime(Request["EndDate"]);

                if (Date2 < Date1)
                {
                    ViewBag.DateValidation = "Start Date cannot be greater than End Date";
                    return View(events);
                }
                else
                {
                    db.Events.Add(events);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
               
            }

            return View(events);
        }

        // GET: Events/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Events events = await db.Events.FindAsync(id);
            ViewBag.StartDate = events.StartDate;
            ViewBag.EndDate = events.EndDate;
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "EventsID,EventType,EventDescription,Venue,StartDate,EndDate")] Events events)
        {
            DateTime Date1 = new DateTime();
            DateTime Date2 = new DateTime();
            if (ModelState.IsValid)
            {
                Date1 = Convert.ToDateTime(Request["StartDate"]);
                Date2 = Convert.ToDateTime(Request["EndDate"]);

                if (Date2 < Date1)
                {
                    ViewBag.DateValidation = "End Date cannot be greater than StartDate";
                    ViewBag.StartDate = events.StartDate;
                    ViewBag.EndDate = events.EndDate;
                    return View(events);
                }
                else
                {
                    db.Entry(events).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.StartDate = events.StartDate;
            ViewBag.EndDate = events.EndDate;
            return View(events);
        }

        // GET: Events/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Events events = await db.Events.FindAsync(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Events events = await db.Events.FindAsync(id);
            db.Events.Remove(events);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
