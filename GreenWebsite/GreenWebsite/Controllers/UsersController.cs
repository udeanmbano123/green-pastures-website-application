﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using GreenWebsite.DAL;
using GreenWebsite.DAL.security;
using GreenWebsite.Models;
using PagedList;
using WebMatrix.WebData;

namespace GreenWebsite.Controllers
{  //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class UsersController : Controller
    {
        private GreenContext db = new GreenContext();

        // GET: Users
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, string Date1String, string Date2String,
            int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.EmailSortParm = String.IsNullOrEmpty(sortOrder) ? "email" : "";

            string name = WebSecurity.CurrentUserName;
            var users = from s in db.Users
                        where s.Email != name
                       select s;

            if (searchString != null || (Date1String != null && Date2String != null))
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(s =>
                  s.Email.ToUpper().Contains(searchString.ToUpper())
                  ||
                  s.Email.ToUpper().Contains(searchString.ToUpper()));
            }

            else if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
                 users = from s in db.Users
                                where s.Email != name && date <= s.CreateDate && date2 >= s.CreateDate
                                select s;



            }
            switch (sortOrder)
            {
                case "name_desc":
                    users = users.OrderByDescending(s => s.Username);
                    break;

                case "Date":
                    users = users.OrderBy(s => s.CreateDate);
                    break;
                case "date_desc":
                    users = users.OrderByDescending(s => s.CreateDate);
                    break;
                default:
                    users = users.OrderBy(s => s.UserID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(users.ToPagedList(pageNumber, pageSize));
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,Username,Email,Password,ConfirmPassword,FirstName,LastName,IsActive,CreateDate,memberNumber,IDNO")] User user)
        {
            if (ModelState.IsValid)
            {
                if (Request["Roles"] == "Admin")
                {
                    Role role1 = new Role { RoleName = "Admin" };

                    user.Roles = new List<Role>();
                    user.Roles.Add(role1);
                    user.memberNumber = "ADM:"+user.Email;
                }
                else if (Request["Roles"] == "User")
                {
                    Role role2 = new Role { RoleName = "User" };
                    user.Roles = new List<Role>();
                    user.Roles.Add(role2);
                }
                user.Password = Request["Password"].ToString();
                user.ConfirmPassword = Request["ConfirmPassword"].ToString();
                user.Password = ComputeHash(user.Password, new SHA256CryptoServiceProvider());
                user.ConfirmPassword = ComputeHash(user.ConfirmPassword, new SHA256CryptoServiceProvider());
                user.IsActive = true;
                user.CreateDate = DateTime.Now;
                db.Users.Add(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            user.Password = "";
            user.ConfirmPassword = "";
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,Username,Email,Password,ConfirmPassword,FirstName,LastName,IsActive,CreateDate,memberNumber,IDNO")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Password = Request["Password"].ToString();
                user.ConfirmPassword = Request["ConfirmPassword"].ToString();
                user.Password = ComputeHash(user.Password, new SHA256CryptoServiceProvider());
                user.ConfirmPassword = ComputeHash(user.ConfirmPassword, new SHA256CryptoServiceProvider());
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges(WebSecurity.CurrentUserName);
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges(WebSecurity.CurrentUserName);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
