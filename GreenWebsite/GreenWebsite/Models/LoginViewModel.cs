﻿//Author:Udean Mbano
//Purpose: Model for the Login Form
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GreenWebsite.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}